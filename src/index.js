import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import App from './App'
import configureStore from './store/configureStore'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './index.css'

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router>
      <App/>
    </Router>
  </Provider>,
  document.getElementById('root'))
registerServiceWorker()
