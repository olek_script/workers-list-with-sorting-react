import { combineReducers } from 'redux'
import workers from './workerReducer'

const rootReducer = combineReducers({
  workers: workers
})

export default rootReducer
