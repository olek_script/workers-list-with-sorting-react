import * as types from './actionTypes'
import workersApi from '../api/workersApi'

export function loadWorkersSuccess (workers) {
  return { type: types.LOAD_WORKERS, workers }
}

export function loadWorkers (page) {
  return function (dispatch) {
    return workersApi.getAllWorkers(page)
      .then((response) => {
        let workers = (response && Array.isArray(response.data)) ? response.data : []
        dispatch(loadWorkersSuccess(workers))
        return workers
      })
      .catch((error) => {
        console.log(`Something went wrong: ${error}. Workers list not loaded.`)
      })
  }
}
