import React from 'react'
import { Link } from 'react-router-dom'

const Fragment = React.Fragment

class AboutPage extends React.Component {
  render () {
    return (
      <Fragment>
        <h1>About</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
            deserunt mollit anim id est laborum.</p>
        <Link to="employees" className="btn btn-primary to-employees-button">To employees list</Link>
      </Fragment>
    )
  }
}

export default AboutPage
