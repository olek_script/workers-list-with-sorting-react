import React from 'react'
import { NavLink } from 'react-router-dom'

const Header = () => {
  return (
    <nav>
      <NavLink to="/employees" activeClassName="active">Employees list</NavLink >
      {' | '}
      <NavLink to="/about" activeClassName="active">About</NavLink >
    </nav>
  )
}

export default Header
