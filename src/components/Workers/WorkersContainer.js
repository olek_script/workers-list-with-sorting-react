import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import lodash from 'lodash'
import InfiniteScroll from 'react-infinite-scroller'

import WorkersList from './WorkersPresentation'
import * as workerActions from '../../actions/workerActions'

const Fragment = React.Fragment

class WorkersContainer extends React.Component {
  state = {
    isDataLoaded: false,
    workers: [],
    sortingOrder: 'asc'
  }
  currentPageNumber = 1
  hasMoreData = true

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      isDataLoaded: true,
      workers: nextProps.workers.length === prevState.workers.length ? prevState.workers : nextProps.workers
    }
  }

  loadNextData = () => {
    if (this.state.sortingOrder === 'desc') { return }

    this.props.actions.loadWorkers(this.currentPageNumber)
      .then((workers) => {
        if (workers && workers.length === 0) {
          this.hasMoreData = false
        }
      })
    this.currentPageNumber = this.currentPageNumber + 1
  }

  sortingOrderChange = (event) => {
    this.setState({
      sortingOrder: event.target.value,
      workers: this.state.workers.slice().reverse()
    })
    this.hasMoreData = event.target.value === 'asc' || false
  }

  render () {
    return (
      <Fragment>
        <div className="employees-header">
          <h3>Employees list</h3>
          <span className="sort-box">
            Sort employees by name:
            <select value={this.state.sortingOrder} onChange={this.sortingOrderChange}>
              <option value="asc">Ascending</option>
              <option value="desc">Descending</option>
            </select>
          </span>
        </div>
        {this.state.workers.length < 1 && this.state && this.state.isDataLoaded &&
          <span>No workers data in storage</span>
        }
        {
          <InfiniteScroll
            pageStart={0}
            loadMore={lodash.debounce(this.loadNextData, 300)}
            hasMore={this.hasMoreData}
            loader={<div className="loader" key={0}>Loading data. Please wait...</div>}
          >
            <WorkersList workers={this.state.workers} />
          </InfiniteScroll>
        }
      </Fragment>
    )
  }
}

WorkersContainer.propTypes = {
  workers: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

function mapStateToProps (state, ownProps) {
  return {
    workers: state.workers
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(workerActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkersContainer)
