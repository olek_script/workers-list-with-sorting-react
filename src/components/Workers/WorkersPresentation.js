import React from 'react'
import PropTypes from 'prop-types'

class WorkersList extends React.Component {
  render () {
    const { workers } = this.props

    return (
      <div id="employees-list" className="row">
        {workers.map((worker, i) =>
          <div key={i} className={`col-5 employee-data ${i % 2 === 0 ? '' : 'offset-1'}`}>
            <div className="row">
              <span className="employee-data-field col-8 col-md-7">
                <span className="employee-name">{worker.last_name + ', ' + worker.first_name}</span>
                <div className="employee-info">
                  <p>E-mail:{` ${worker.email}`}</p>
                  <p>Company:{` ${worker.company}`}</p>
                  <p className="employee-address">Address:{` ${worker.adress}`}</p>
                  <p>Phone:{` ${worker.phone}`}</p>
                </div>
              </span>
              <img className="col-4 col-md-5" src={worker.avatar} alt="-"/>
            </div>
          </div>
        )}
      </div>
    )
  }
}

WorkersList.propTypes = {
  workers: PropTypes.array.isRequired
}

export default WorkersList
