import React, { Component } from 'react'
import { Switch } from 'react-router'
import { Route, Redirect } from 'react-router-dom'
import AboutPage from './components/about/AboutPage'
import EmployeesPage from './components/Workers/WorkersContainer'
import Header from './components/common/Header'
import './App.css'

class App extends Component {
  render () {
    return (
      <div className="container-fluid">
        <Header/>
        <Switch>
          <Route exact={true} path="/" component={EmployeesPage}></Route>
          <Route path="/employees" component={EmployeesPage}/>
          <Route path="/about" component={AboutPage} />
          <Redirect from='*' to='/' />
        </Switch>
      </div>
    )
  }
}

export default App
