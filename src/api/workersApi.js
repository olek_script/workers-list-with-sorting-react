import axios from 'axios'

const port = 3001
const host = `http://localhost:${port}/`
const limit = 16
const sortBy = 'last_name'
const employees = `${host}employees?_limit=${limit}&_sort=${sortBy}`

class WorkersApi {
  static getAllWorkers (page) {
    return axios.get(`${employees}&_page=${page}`, { port: port })
  }
}

export default WorkersApi
